function sortCarByYearAscendingly(cars) {
  // Sangat dianjurkan untuk console.log semua hal hehe
  // console.log(cars);

  // console.log('cek javascript');
  // Clone array untuk menghindari side-effect
  // Apa itu side effect?
  const result = [...cars];

  // Tulis code-mu disini
  // 1. looping untuk cek data satu per satu.
  for (var i = 0; i < result.length; i++){
    // 2. looping utk before index i
    for (var j=0; j < result.length - 1; j++){
      // compare antara data index i dan data index j
      // logika untuk membandingkan
      if (result[j].year > result[j + 1].year){
        // buat temporary object
        let tempObj = result[j]
        // masukin data yang lebih besar
        result[j] = result[j + 1]
        result[j + 1] = tempObj
      }
    }
  }

  // Rubah code ini dengan array hasil sorting secara ascending
  return result;
}

sortCarByYearAscendingly(cars)
